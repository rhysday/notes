##################################
# 1. Arch install preperation #
##################################
1.1. Download iso VIA TORRENT from https://www.archlinux.org

1.2. Flash iso to USB:
Confirm name of USB stick
$ lsblk -f

if sdc1 partition, then use sdc

If iso is located in the home directory and copying to USB sdc:

sudo umount /dev/sdb
sudo dd if=/home/me/Downloads/archlinux-2021.07.01-x86_64.iso of=/dev/sdb bs=4M && sync
################################


1.3. !!! Boot USB with it connected directly into the back of the computer !!!

1.4. In installer menu, select first option in menu.

1.5. Connect to internet
Use wired internet connection
$ ping archlinux.org


################################
# 2. Installation prerequisites #
################################
https://wiki.archlinux.org/index.php/installation_guide


2.1. Keyboard
Set keyboard on Architect ISO by using loadkeys followed by a country code e.g. for U.S.A
# loadkeys us

2.2. Update the System Clock
# timedatectl set-ntp true

########################
# 3. Partitioning disk #
########################
List disk devices
# lsblk

Partition layout
1. efi - 1G - FAT32
2. swap - 64G (or equal to system RAM size) - !!! Changed to 32G. 16G for Thinkpad.
3. root - 100G - !!! Changed to 60G. 50G for Thinkpad.
4. home - remaining space - !!! Changed to 300G

The following will use disk sda as example. Change to correct disk label.

Note: This command uses the –zero argument to empty the partition table of the selected disk.
# cfdisk --zero /dev/sda

Select gpt label type when prompted and press Enter

1. EFI
Select Free space using ↑ or ↓ .
Select New using ← or → → Enter .
1G press Enter .
Select Type → Enter .
Select EFI System → Enter .

2. swap partition.
Select Free space → New → Enter .
Enter size 64G → Enter .
Select Type → Enter .
Select Linux swap → Enter .

3. root partition.
Select Free space → New &rarr Enter .
Enter size 100G → Enter
Select Type → Enter .
Select Linux root (x86_64) → Enter .

4. home partition.
Select Free space → New → Enter .
Enter to assign the remaining space to home.
Select Type → Enter .
Select Linux home → Enter .

5. Save the changes
Select Write → Enter
Input yes when prompted → Enter
Select Quit → Enter


#################
# 4. Formatting #
#################
# lsblk

EFI ($esp)
# mkfs.fat -F 32 /dev/sda1

Swap
# mkswap /dev/sda2

Root
# mkfs.ext4 /dev/sda3

Home
# mkfs.ext4 /dev/sda4


###############
# 5. Mounting #
###############

Before we install the base system we need to mount the devices. First we mount the root system - using the
folder /mnt for the temporary mount, then we make folders for /boot/efi and /home.
# mount /dev/sda3 /mnt
# mkdir -p /mnt/boot
# mkdir -p /mnt/home

Verify your folder structure
# find /mnt -type d
/mnt
/mnt/home
/mnt/boot

Mount the efi and home partition
# mount /dev/sda1 /mnt/boot
# mount /dev/sda4 /mnt/home

Verify your mounts
# lsblk


########################
# 6. Base installation #
########################
Visit www.kernel.org for latest kernel information.

Update mirrors list:
# reflector --country Australia --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

# pacstrap /mnt base linux linux-firmware dhcpcd networkmanager neovim vi vim sudo links amd-ucode (intel-ucode for Thinkpad)

# genfstab -U /mnt >> /mnt/etc/fstab


#########################
# 7. Base configuration #
#########################
Next thing is to configure the system in a chrooted environment. The tasks to be performed creating the
necessary configurations for you new system. We will use bash as shell.

# arch-chroot /mnt /bin/bash

Locale
To generate the messages edit: 
/etc/locale.gen 
and remove the comment for locale(s) to be generated (UTF8 is the recommend choice).

Select locale
Example for a system in Australia using english messages
...
#en_CA ISO-8859-1
en_AU.UTF-8 UTF-8
en_AU ISO-8869-1
...
TIP: uncomment the locale en_US.UTF-8 UTF-8 as a fallback locale

Generate the messages
# locale-gen

locale.conf
Edit your locale configuration in /etc/locale.conf to match above choice - example for Australia

LANG=en_AU.UTF-8


Timezone

Set the time zone for location (the available zones is listed in /usr/share/zoneinfo/ using the
Continent/Capitol format).
Symlink the time zone as /etc/localtime - example for Australia
# ln -sf /usr/share/zoneinfo/Australia/Brisbane /etc/localtime


Clock

Linux clock runs using the timezone info and UTC time.
# hwclock --systohc


Internet

Don’t enable both - not necessary and not a good idea.

Either enable basic ethernet using dhcpcd
# systemctl enable dhcpcd

or Network Manager if you need e.g. wireless
# systemctl enable NetworkManager



Hostname

Set hostname
# echo desktop > /etc/hostname


Hosts configuration
# nvim /etc/hosts

127.0.0.1 	localhost
::1 		localhost
127.0.1.1 	desktop.localdomain desktop

Note: If the system has a static IP replace 127.0.1.1 with the IP.


System administration

Allow members of the wheel group to perform administrative tasks.
Run visudo

# visudo
Locate the line reading # %wheel ALL=(ALL) ALL and remove the # in the beginning of the line
%wheel ALL=(ALL) ALL


Root password
Set a password the root user
# passwd


#################
# 8. Bootloader #
#################
# pacman -S grub efibootmgr

Bootloader 
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB

Generate grub configuration
# grub-mkconfig -o /boot/grub/grub.cfg


#################
# 9. Conclusion #
#################

Close chroot
# exit

Unmount the partitions
# umount -R /mnt

Restart your system
Remember to remove your install media.
# reboot


###########################
# 10. New Arch Install #
###########################

10.1 Login as root
username: root
passwd: yubikey


To use the multilib repository, uncomment the [multilib] section in /etc/pacman.conf (Please be sure to uncomment both lines):

[multilib]
Include = /etc/pacman.d/mirrorlist

!!! Then update the package list and upgrade with pacman -Syu !!!


10.2 Important Packages:
pacman-S linux-headers
pacman -S xorg 
pacman -S nvidia 
pacman -S cuda


10.3 Installer Script
pacman-key --refresh-keys
curl -LO https://gitlab.com/rhysday/auto-rice-desktop/-/raw/master/installer.sh
!!! THINKPAD do auto-rice-laptop in URL instead.
run $ sh installer.sh


Mount Drives
# lsblk -f
# mkdir /mnt/10TB
# mount -U UUID /mnt/10TB

Check fstab file in /mnt/10TB/Backups/system/fstab 
Make sure the system partitions are the same as in /etc/fstab that was created by fstabgen. Then overwrite. 
DO NOT OVERWRITE /etc/fstab unless the information already on it is in the backup version.

!!! Also make sure the UUID's in lsblk -f match those in the fstab !!!

# mkdir /mnt/Projects
# mkdir /mnt/Scratch
# mkdir /mnt/Backups

# reboot





